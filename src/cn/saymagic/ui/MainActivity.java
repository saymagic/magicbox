package cn.saymagic.ui;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import cn.saymagic.bean.PropertyBean;
import cn.saymagic.service.MusicService;


public class MainActivity extends Activity {

	// 显示组件
	private ImageButton imgBtn_Previous;
	private ImageButton imgBtn_PlayOrPause;
	private ImageButton imgBtn_Stop;
	private ImageButton imgBtn_Next;
	private ListView list;
	private TextView text_current;
	private TextView text_duration;
	private SeekBar seekBar;
	private RelativeLayout rootLayout;
	//更新进度条的handler
	private Handler seekBarHandler;
	//当前歌曲的持续时间和当前位置
	private int duration;
	private int time;
	//进度条控制常量
	private static final int Progress_ICREASE = 0;
	private static final int PROGRESS_PAUSE = 1;
	private static final int PROGRESS_RESET =  2;
	//当前歌曲id，
	private long id;
	//当前歌曲序号
	private int number;

	// 播放状态
	private int status;
	// 广播接收器
	private StatusChangedReceiver receiver;


	//menu常量
	public static final int MENU_THEME = Menu.FIRST;
	public static final int MENU_ABOUT = Menu.FIRST+1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		findViews();
		registerListeners();
		number = 0;
		id=list.getItemIdAtPosition(number);
		status = MusicService.STATUS_STOPPED;
		startService(new Intent(this, MusicService.class));
		// 绑定广播接收器，可以接收广播
		bindStatusChangedReceiver();
		// 检查播放器是否正在播放。如果正在播放，以上绑定的接收器会改变UI
		sendBroadcastOnCommand(MusicService.COMMAND_CHECK_IS_PLAYING);	
		initSeekBarHandler();

	}

	/** 获取显示组件 */
	private void findViews() {
		imgBtn_Previous = (ImageButton) findViewById(R.id.imageButton1);
		imgBtn_PlayOrPause = (ImageButton) findViewById(R.id.imageButton2);
		imgBtn_Stop = (ImageButton) findViewById(R.id.imageButton3);
		imgBtn_Next = (ImageButton) findViewById(R.id.imageButton4);
		seekBar = (SeekBar) findViewById(R.id.seekBar1);
		list = (ListView) findViewById(R.id.listView1);
		text_current = (TextView) findViewById(R.id.textView1);
		text_duration = (TextView) findViewById(R.id.textView2);
		rootLayout = (RelativeLayout) findViewById(R.id.relativeLayout1);
	}

	/** 为显示组件注册监听器 */
	private void registerListeners() {
		imgBtn_Previous.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				sendBroadcastOnCommand(MusicService.COMMAND_PREVIOUS);
			}
		});
		imgBtn_PlayOrPause.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				if (isPlaying()) {
					sendBroadcastOnCommand(MusicService.COMMAND_PAUSE);
				} else if (isPaused()) {
					sendBroadcastOnCommand(MusicService.COMMAND_RESUME);
				} else if (isStopped()) {
					sendBroadcastOnCommand(MusicService.COMMAND_PLAY);
				}
			}
		});
		imgBtn_Stop.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				sendBroadcastOnCommand(MusicService.COMMAND_STOP);
			}
		});
		imgBtn_Next.setOnClickListener(new OnClickListener() {
			public void onClick(View view) {
				sendBroadcastOnCommand(MusicService.COMMAND_NEXT);
			}
		});
		list.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long _id) {
				number = position;
				id = _id;
				//Toast.makeText(getApplicationContext(), "position:   "+position+"   id:  "+id, 3000).show();
				Log.d("position", ""+position);
				Log.d("id", ""+id);				
				//number = position + 1;
				sendBroadcastOnCommand(MusicService.COMMAND_PLAY);

			}
		});
		seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
				//发送广播给musicservice.执行跳转
				sendBroadcastOnCommand(MusicService.COMMAND_SEEK_TO);
				if(isPlaying())
					seekBarHandler.sendEmptyMessageDelayed(Progress_ICREASE, 1000);
			}

			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
				seekBarHandler.sendEmptyMessage(PROGRESS_PAUSE);
			}

			@Override
			public void onProgressChanged(SeekBar arg0, int progress, boolean arg2) {
				time = progress;
				//更新文本
				text_current.setText(formatTime(time));

			}
		});
	}

	private void initSeekBarHandler(){
		seekBarHandler = new Handler(){

			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				switch (msg.what) {
				case Progress_ICREASE:
					if(seekBar.getProgress()<duration){
						System.out.print("Progress_ICREASE");
						//进度条前进一秒
						seekBar.incrementProgressBy(1000);
						seekBarHandler.sendEmptyMessageDelayed(Progress_ICREASE, 1000);

						text_current.setText(formatTime(time));
						time+=1000;
					}
					break;
				case PROGRESS_PAUSE:
					seekBarHandler.removeMessages(Progress_ICREASE);
					break;
				case PROGRESS_RESET:
					seekBarHandler.removeMessages(Progress_ICREASE);
					seekBar.setProgress(0);
					text_current.setText("00:00");
					break;
				default:
					break;
				}
			}

		};
	}

	/** 绑定广播接收器 */
	private void bindStatusChangedReceiver() {
		receiver = new StatusChangedReceiver();
		IntentFilter filter = new IntentFilter(
				MusicService.BROADCAST_MUSICSERVICE_UPDATE_STATUS);
		registerReceiver(receiver, filter);
	}

	//获得歌曲信息
	private String getSongMsgById(long number){
		ContentResolver resolver = getContentResolver();
		Cursor cursor = resolver.query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, null, MediaStore.Audio.AudioColumns._ID +"="+number , null, null);		
		String s = "";
		if(cursor.moveToNext()){
			String tilte = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.TITLE));  
			String artist = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.ARTIST));  
			Log.d(tilte, artist);
			
			s = "当前播放歌曲: "+tilte+"；   演唱者："+artist;

		}
		return s;
	}
	
	/** 内部类，用于播放器状态更新的接收广播 */
	class StatusChangedReceiver extends BroadcastReceiver {
		public void onReceive(Context context, Intent intent) {
			// 获取播放器状态
			status = intent.getIntExtra("status", -1);
			switch (status) {
			case MusicService.STATUS_PLAYING:
				time = intent.getIntExtra("time", 0);
				duration = intent.getIntExtra("duration", 0);
				seekBarHandler.removeMessages(Progress_ICREASE);
				seekBarHandler.sendEmptyMessageDelayed(Progress_ICREASE,1000);
				seekBar.setMax(duration);
				seekBar.setProgress(time);
				text_duration.setText(formatTime(duration));
				imgBtn_PlayOrPause.setBackgroundResource(R.drawable.pause);
				//设置标题栏的文字
				MainActivity.this.setTitle(getSongMsgById(id));
				break;
			case MusicService.STATUS_PAUSED:
				seekBarHandler.sendEmptyMessage(PROGRESS_PAUSE);
				imgBtn_PlayOrPause.setBackgroundResource(R.drawable.play);
				break;
			case MusicService.STATUS_STOPPED:
				seekBarHandler.sendEmptyMessage(PROGRESS_RESET);
				MainActivity.this.setTitle("GracePlayer");
				imgBtn_PlayOrPause.setBackgroundResource(R.drawable.play);
				break;
			case MusicService.STATUS_COMPLETED:
				sendBroadcastOnCommand(MusicService.COMMAND_NEXT);
				seekBarHandler.sendEmptyMessage(PROGRESS_RESET);
				MainActivity.this.setTitle("MagicPlayer");
				imgBtn_PlayOrPause.setBackgroundResource(R.drawable.play);
				break;
			default:
				break;
			}
		}
	}



	/*
	 * 发送命令，控制音乐播放，参数定义在MusicService中！
	 */
	private void sendBroadcastOnCommand(int command){
		Intent intent = new Intent(MusicService.BROADCAST_MUSICSERVICE_CONTROL);
		Bundle bl = new Bundle();
		bl.putInt("command", command);
		//intent.putExtra("command", command);
		//根据不同命令
		switch(command){
		case MusicService.COMMAND_PLAY:
			Log.d(" sendBroadcastOnCommand id", ""+id);
			bl.putLong("id", id);
			break;
		case MusicService.COMMAND_PREVIOUS:
			moveNumberToPrevious();
			bl.putLong("id", id);
			break;
		case MusicService.COMMAND_NEXT:
			moveNumberToNext();
			bl.putLong("id", id);
			break;
		case MusicService.COMMAND_SEEK_TO:
			Log.d("COMMAND_SEEK_TO time", time+"");
			bl.putInt("time", time);
			break;
		case MusicService.COMMAND_PAUSE:
		case MusicService.COMMAND_STOP:
		case MusicService.COMMAND_RESUME:		
		default:
			break;

		}
		intent.putExtras(bl);
		sendBroadcast(intent);
	}

	/** 创建菜单 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, MENU_THEME, 0, "主题");
		menu.add(0, MENU_ABOUT, 1, "关于");
		return super.onCreateOptionsMenu(menu);
	}

	/** 处理菜单点击事件 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case MENU_THEME:
			// 显示列表对话框
			new AlertDialog.Builder(this)
			.setTitle("请选择主题")
			.setItems(R.array.theme,
					new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,
						int which) {
					// 保存选择的主题
					PropertyBean property = new PropertyBean(
							MainActivity.this);
					// 获取在array.xml中定义的主题名称
					String theme = PropertyBean.THEMES[which];
					// 设置Activity的主题
					setTheme(theme);

					property.setAndSaveTheme(theme);
				}
			}).show();
			break;
		case MENU_ABOUT:
			// 显示文本对话框
			new AlertDialog.Builder(this).setTitle("MusicPlayer")
			.setMessage(this.getString(R.string.about)).show();
			break;
		}
		return super.onOptionsItemSelected(item);
	}


	private String formatTime(int msec){
		int minute = (msec/1000)/60;
		int second = (msec/1000)%60;
		String minutesString;
		String secondString;
		if(minute<10)
			minutesString = "0"+minute;
		else
			minutesString = ""+minute;

		if(second<10)
			secondString = "0" + second;
		else
			secondString = ""+second;

		return minutesString+":"+secondString;
	}

	private void moveNumberToNext(){
		//判断是否达到了列表底端 
		if((number+1)>=list.getCount()){
			Toast.makeText(this, this.getString(R.string.tip_reach_bottom), Toast.LENGTH_SHORT).show();
			number = 0;
		}else{
			++number;
			//播放音乐需要用到的是id，但id是不固定的，所以我们只能根据number来求id
;
		}
		id=list.getItemIdAtPosition(number);

	}


	private void moveNumberToPrevious(){
		//判断是否达到了列表顶端
		if(number==0){
			Toast.makeText(this, this.getString(R.string.tip_reach_top), Toast.LENGTH_SHORT).show();
			number = list.getCount()-1;

		}else{
			--number;
		}
		id=list.getItemIdAtPosition(number);

	}



	@Override
	protected void onResume() {
		super.onResume();
		// 初始化音乐列表
		initMusicList();
		// 如果列表没有歌曲，则播放按钮不可用，并提醒用户
		if (list.getCount() == 0) {
			imgBtn_Previous.setEnabled(false);
			imgBtn_PlayOrPause.setEnabled(false);
			imgBtn_Stop.setEnabled(false);
			imgBtn_Next.setEnabled(false);
			Toast.makeText(this, this.getString(R.string.tip_no_music_file),
					Toast.LENGTH_SHORT).show();
		} else {
			imgBtn_Previous.setEnabled(true);
			imgBtn_PlayOrPause.setEnabled(true);
			imgBtn_Stop.setEnabled(true);
			imgBtn_Next.setEnabled(true);
		}
	}

	/** 初始化音乐列表。包括获取音乐集和更新显示列表 */
	private void initMusicList() {
		Cursor cursor = getMusicCursor();
		setListContent(cursor);
	}

	/** 更新列表的内容 */
	private void setListContent(Cursor musicCursor) {
		CursorAdapter adapter = new SimpleCursorAdapter(this,
				android.R.layout.simple_list_item_2, musicCursor, new String[] {
				MediaStore.Audio.AudioColumns.TITLE,
				MediaStore.Audio.AudioColumns.ARTIST }, new int[] {
				android.R.id.text1, android.R.id.text2 });
		list.setAdapter(adapter);
	}

	/**
	 *  获取系统扫描得到的音乐媒体集 
	 * 记得加权限
	 * */
	private Cursor getMusicCursor() {
		// 获取数据选择器
		ContentResolver resolver = getContentResolver();
		// 选择音乐媒体集
		Cursor cursor = resolver.query(
				MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, null, null, null,
				null);
		return cursor;
	}

	/** 是否正在播放音乐 */
	private boolean isPlaying() {
		return status == MusicService.STATUS_PLAYING;
	}

	/** 是否暂停了播放音乐 */
	private boolean isPaused() {
		return status == MusicService.STATUS_PAUSED;
	}

	/** 是否停止状态 */
	private boolean isStopped() {
		return status == MusicService.STATUS_STOPPED;
	}

	private void setTheme(String theme){
		if(theme.equals("彩色")){
			rootLayout.setBackgroundResource(R.drawable.bg_color);
		}else if(theme.equals("花朵")){
			rootLayout.setBackgroundResource(R.drawable.bg_digit_flower);

		}else if(theme.equals("群山")){
			rootLayout.setBackgroundResource(R.drawable.bg_mountain);

		}else if(theme.equals("小狗")){
			rootLayout.setBackgroundResource(R.drawable.bg_running_dog);

		}else if(theme.equals("冰雪")){
			rootLayout.setBackgroundResource(R.drawable.bg_snow);

		}else if(theme.equals("女孩")){
			rootLayout.setBackgroundResource(R.drawable.bg_music_girl);

		}else if(theme.equals("朦胧")){
			rootLayout.setBackgroundResource(R.drawable.bg_blur);

		}

	}
}
